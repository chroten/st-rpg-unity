﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class OnlineManager : MonoBehaviour {

    // When connecting to your own port it seems to Connect twice.
    // Surely a disconnect should do 2 disconnects then?

    public MultiplayerUI ui;
    public NetworkInterface network;
    private bool someoneIsHosting = false;
    private bool updatingHostAddress = false;
    private const int MAX_PLAYERS = 6;


    // Use this for initialization
    void Start() {

        // Get msg from other machines to check host if host exists here <----

        ui = Instantiate(ui) as MultiplayerUI;
        network = Instantiate(network) as NetworkInterface;

        network.InitializeNetwork();

        network.Initialize(MAX_PLAYERS);
        ui.Initialize(MAX_PLAYERS);

        if (!someoneIsHosting) {

            network.CreateHostOnMyMachine();
            ui.SetCurrentHostText(network.GetHostAddress());
            someoneIsHosting = true;
        }

        else network.ConnectToCurrentHost();


    }

    // Update is called once per frame

    public void AddPlayerToGame() {

        if (!network.IsGameFull()) {
            network.ConnectToCurrentHost();
            ui.AddNewPlayerIcon(network.GetPlayerCount());
            network.IncrementPlayerCount();
        }
    }

    public void RemovePlayerFromGame() {

        if (!network.IsGameEmpty()) {
            network.DisconnectFromCurrentHost();
            ui.RemoveLastPlayerIcon(network.GetPlayerCount());
            network.DecrementPlayerCount();
        }
    }

    public bool IsEnteringAddress() {
        return ui.IsUIInFocus();
    }


    void Update() {

        if (IsEnteringAddress()) updatingHostAddress = true;

        if (updatingHostAddress) {
            if (!IsEnteringAddress()) {
                network.SetHostAddress(ui.GetCurrentHostEntry());
                //network.CreateHost(ui.GetCurrentHostEntry());
                updatingHostAddress = false;
            }
        }

        //network.SetHostAddress(ui.GetCurrentHostEntry()); // This is in severe need of an overhaul
        //network.ReceiveMessageFromHost();

    }

}
