﻿using UnityEngine;
using System.Collections;

public class Keyboard : InterfaceMonoBehaviour {

    private Inventory inventory;
    private OnlineManager onlineManager;

    private bool isTextFieldBeingEdited = false; // Need to use this or a global equivalent at some point as opposed to where I put the second *..

    public override void Initialize(UnityEngine.Object[] objectList) {

        foreach (Object obj in  objectList) {

            if (obj is OnlineManager) {
                onlineManager = (OnlineManager)obj;
            }

            if (obj is Inventory) {
                inventory = (Inventory)obj;
            }
        }
    }

    // Update is called once per frame
    void Update() {


        if (!onlineManager.IsEnteringAddress()) { // ...* here!

            if (Input.GetKey(KeyCode.W)) {
                localPlayer.MoveUp();
            }

            if (Input.GetKey(KeyCode.S)) {
                localPlayer.MoveDown();
            }

            if (Input.GetKey(KeyCode.A)) {
                localPlayer.MoveLeft();
            }

            if (Input.GetKey(KeyCode.D)) {
                localPlayer.MoveRight();
            }

            if (Input.GetKeyDown(KeyCode.Alpha1)) {
                onlineManager.AddPlayerToGame();
            }

            if (Input.GetKeyDown(KeyCode.Alpha2)) {
                onlineManager.RemovePlayerFromGame();
            }

            if (Input.GetKeyDown(KeyCode.I)) {
                inventory.Toggle();
            }

            if (Input.GetKeyDown(KeyCode.Escape)) {
                Debug.Log("ESCAPE == MENU??!?! OPENED");
            }

            if (Input.GetKeyDown(KeyCode.T)) {
                Debug.Log("opened the chat!");
            }

            if (Input.GetKeyDown(KeyCode.C)) {
                Debug.Log("show character stats and such");
            }
        }
    }
}
