﻿using UnityEngine;
using System.Collections;

public class GameSettings : MonoBehaviour
{

    private const int MAX_UNIQUE_OBJECTS = 5;
    private UnityEngine.Object[] objectList;

    private bool initialize = false;


    public Keyboard keyboard;
    public Inventory inventory;
    public OnlineManager onlineManager;
    public Camera cam;
    public Player localPlayer;
    public Texture2D texture;  // Your cursor texture
    int x = 16;  // Your cursor size x
    int y = 16;  // Your cursor size y

    Vector3 playerStartPosition;
    Vector3 camStartPosition;
    Vector3 playerCamDist;

    void Start()
    {
        objectList = new UnityEngine.Object[MAX_UNIQUE_OBJECTS];
        keyboard = Instantiate(keyboard) as Keyboard;
        onlineManager = Instantiate(onlineManager) as OnlineManager;
        localPlayer = Instantiate(localPlayer) as Player;
        cam = Instantiate(cam) as Camera;

        onlineManager.transform.SetParent(transform);

        objectList[0] = inventory;
        objectList[1] = onlineManager;

        playerStartPosition = new Vector3(205,30,210);
        camStartPosition = new Vector3(-704, 450, 200);
        playerCamDist = camStartPosition - playerStartPosition;

        cam.tag = "MainCamera";
        cam.enabled = true;
        cam.transform.position = camStartPosition;

        
        localPlayer.Initialize(cam, objectList);
        localPlayer.SetPosition(playerStartPosition);

        keyboard.Initialize(localPlayer, objectList);

        
        inventory.Toggle();
       // inventory.Initialize(objectList);

    }

    void Update() {

        if (!initialize) {
            inventory.Initialize(null);
            initialize = true;
        }
        
    }

    void OnGUI()
    {
        if (Event.current.mousePosition.x >= 0 && Event.current.mousePosition.x <= Screen.width &&
            Event.current.mousePosition.y >= 0 && Event.current.mousePosition.y <= Screen.height)
        {

            Cursor.visible = false;
            GUI.DrawTexture(new Rect(Event.current.mousePosition.x - x / 2, Event.current.mousePosition.y - y / 2, x, y), texture);
        }

        else Cursor.visible = true;
    }




    
}
