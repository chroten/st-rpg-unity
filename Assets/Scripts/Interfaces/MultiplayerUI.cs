﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MultiplayerUI : MonoBehaviour {


    Image[] iconArray;
    public Image connectionIcon;
    private int MAX_PLAYERS;

    // Use this for initialization
    void Start () {

        iconArray = new Image[MAX_PLAYERS];
        
    }

    public void Initialize(int players) {
        MAX_PLAYERS = players;
    }
        

    public void SetCurrentHostText(string address) {
        transform.GetComponentInChildren<InputField>().text = address;
    }

    public bool IsUIInFocus() {

        return transform.GetComponentInChildren<InputField>().isFocused;
    }

    public void AddNewPlayerIcon(int playerCount) {

        iconArray[playerCount] = Instantiate(connectionIcon) as Image;
        iconArray[playerCount].transform.SetParent(transform, false);
        iconArray[playerCount].transform.Translate(new Vector3(playerCount * 35, 0, 0));

    }

    public string GetCurrentHostEntry() {
        return transform.GetComponentInChildren<InputField>().text;
    }

    public void RemoveLastPlayerIcon(int playerCount) {

        Destroy(iconArray[playerCount - 1]);

    }

}
