﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/*
 * TODO:
 * 
 * Make tooltip reusable for everything, not just items in the future. Maybe make proper use of Scriptable Objects?
 * 
 * This entire class needs to be reused for any kind of tool tip in the game, so every item, building, skill and so on has
 * to be able to use this exact tooltip.
 */


public class Tooltip : ExtendedMonoBehaviour {

    private Item item;
    private string data;
    private GameObject tooltip;
    private Camera cam;

    void Start() {
        Initialize(null);
    }

    public override void Initialize(UnityEngine.Object[] objectList) {

        tooltip = GameObject.Find("Tooltip");
        cam = GameObject.Find("Isometric View Camera(Clone)").GetComponent<Camera>();
        tooltip.SetActive(false);

    }

    void Update() {
        if (tooltip.activeSelf) { 
            var screenPoint = Input.mousePosition;
            screenPoint.z = 100.0f; //distance of the plane from the camera
            tooltip.transform.position = cam.ScreenToWorldPoint(screenPoint);
        }
    }

    public void Activate(Item item) {
        this.item = item;
        ConstructDataString();
        tooltip.SetActive(true);
    }

    public void Deactivate() {
        tooltip.SetActive(false);
    }

    /// <summary>
    /// Construct a string with data
    /// 
    /// Note that the strings can be styled with HTML tags
    /// </summary>
    public void ConstructDataString() {
        data = "<color=#ffb526><b>" + item.Title + "</b></color>\n\n"
            + item.Description + "\n\nPower: " + item.Power;
        tooltip.transform.GetChild(0).GetComponent<Text>().text = data;
    }
}
