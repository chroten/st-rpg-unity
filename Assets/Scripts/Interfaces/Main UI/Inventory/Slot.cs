﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class Slot : MonoBehaviour, IDropHandler {
    public int SlotID { get; set; }
    private Inventory inventory;

    void Start() {
        inventory = GameObject.Find("Inventory Panel").GetComponent<Inventory>();
    }

    public void OnDrop(PointerEventData eventData) {
        ItemData droppedItem = eventData.pointerDrag.GetComponent<ItemData>();
        if (inventory.itemList[SlotID].ItemID == -1) {
            inventory.itemList[droppedItem.Slot] = new Item();
            inventory.itemList[SlotID] = droppedItem.Item;
            droppedItem.Slot = SlotID;
        } else if (droppedItem.Slot != SlotID) {
            Transform item = this.transform.GetChild(0);
            item.GetComponent<ItemData>().Slot = droppedItem.Slot;
            item.transform.SetParent(inventory.slotList[droppedItem.Slot].transform);
            item.transform.position = inventory.slotList[droppedItem.Slot].transform.position;

            droppedItem.Slot = SlotID;
            droppedItem.transform.SetParent(this.transform);
            droppedItem.transform.position = this.transform.position;

            inventory.itemList[droppedItem.Slot] = item.GetComponent<ItemData>().Item;
            inventory.itemList[SlotID] = droppedItem.Item;
        }
    }

}
