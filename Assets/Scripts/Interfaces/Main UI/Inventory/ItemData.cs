﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class ItemData : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler {

    public Item Item { get; set; }
    public int Amount { get; set; }
    public int Slot { get; set; }

    private Inventory inventory;
    private Tooltip tooltip;
    private Vector2 offset;

    Camera cam;
    
    void Start() {
        inventory = GameObject.Find("Inventory Panel").GetComponent<Inventory>();
        tooltip = inventory.GetComponent<Tooltip>();
        cam = GameObject.Find("Isometric View Camera(Clone)").GetComponent<Camera>();
    }

    public void OnBeginDrag(PointerEventData eventData) {
        if (Item != null) {
            this.transform.SetParent(this.transform.parent.parent);
            GetComponent<CanvasGroup>().blocksRaycasts = false;
        }
    }

    public void OnDrag(PointerEventData eventData) {
        if (Item != null) {
            var screenPoint = Input.mousePosition;
            screenPoint.z = 100.0f; //distance of the plane from the camera
            transform.position = cam.ScreenToWorldPoint(screenPoint);

            /*
            // Here I store the slot position for safekeeping
            Vector2 slotPosition2d = cam.WorldToScreenPoint(transform.parent.position);
            Vector3 slotPosition3d = transform.parent.position;

            // Here I store the mouse position, and calculate the difference between the 2d positions (x-x, y-y)
          
            Vector2 screenPos = eventData.position;

            // I divide by 10 here because I don't know how to scale the screen properly
            // to match the mouses speed. Although we probably shouldn't be going anywhere
            // near this method. We need to syncronise the mouse and item, not just scale.

            Vector2 newPos = (screenPos - slotPosition2d) / 10;

            // The new position = Original slot position + the difference between the slots y and the mouses new y * by the cameras up vector
            //+ the difference between the slots x and the mouses new x * by the cameras right vector

            // The default up vector for all objects is    (0, 1, 0), meaning Y is UP
            // The default right vector for all objects is (1, 0 ,0), meaning X is RIGHT

            // Whenever you rotate an object, these will change - if you rotated the camera 45 degrees from (0, 1, 0) it's up might look like (0.5, 0.5, 0) (don't ask me I don't actually know
            // how to do the math properly... )

            // So, whenever you're doing stuff in the view of the camera e.g. moving an item you must take into account what is UP or RIGHT to the camera. Otherwise when you
            // move an object it's going to take the default values. I'll add a debug to show you what UP and RIGHT is for the camera.

            

            transform.position = slotPosition3d + (cam.transform.up * newPos.y) + (cam.transform.right * newPos.x);


        
            //Debug.Log("Item position:  " + transform.position);
            //Debug.Log("Mouse position: " + eventData.position);
            //Debug.Log("Cameras Up Vector: " + cam.transform.up);
            //Debug.Log("Default Up Vector: " + Vector3.up);

            //Debug.Log("original parent: " + originalParent.transform.position);
            //Debug.Log("original position: " + transform.position);
            */
        }
    }

    public void OnEndDrag(PointerEventData eventData) {
        this.transform.SetParent(inventory.slotList[Slot].transform);
        this.transform.position = inventory.slotList[Slot].transform.position;
        GetComponent<CanvasGroup>().blocksRaycasts = true;
    }

    public void OnPointerEnter(PointerEventData eventData) {
        tooltip.Activate(Item);
    }

    public void OnPointerExit(PointerEventData eventData) {
        tooltip.Deactivate();
    }
}
