﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Inventory : ExtendedMonoBehaviour {

    /*
     * TODO:
     * instead of asigning the gameobjects in the inspector, maybe do a resource.load?
     * not sure if that is better, needs to be researched later
     * 
     * Find out whether it's better to disable rendering upon inventory toggle or to keep it as it is...
     * by putting the alpha (opacity) to 0 or 1.
     * 
     * ItemInInventory() feels like it is not needed... Look at this with more detail later? less loops is better pls
     */

    GameObject inventoryPanel;
    GameObject slotPanel;
    public GameObject inventorySlot;
    public GameObject inventoryItem;

    ItemDatabase database;

    private int slotAmount;
    public List<Item> itemList = new List<Item>();
    public List<GameObject> slotList = new List<GameObject>();

    public override void Initialize(UnityEngine.Object[] objectList) {

        // disable display upon start
        transform.GetComponent<CanvasGroup>().interactable = false;
        transform.GetComponent<CanvasGroup>().alpha = 0;

        database = GetComponent<ItemDatabase>();
        slotAmount = 25;
        inventoryPanel = GameObject.Find("Inventory Panel");
        slotPanel = inventoryPanel.transform.FindChild("Slot Panel").gameObject;

        // create inventoryslots and give each slot an ID.
        for (int i = 0; i < slotAmount; ++i) {
            itemList.Add(new Item());
            slotList.Add(Instantiate(inventorySlot));
            slotList[i].GetComponent<Slot>().SlotID = i;
            slotList[i].transform.SetParent(slotPanel.transform, false);
        }

        AddItem(0);
        AddItem(1);
        AddItem(1);
        AddItem(1);
        AddItem(1);
        AddItem(1);
        AddItem(1);
        AddItem(1);
        AddItem(0);

    }

    void Start () {
        //Initialize(null);
    }

    /// <summary>
    /// AddItem serves for adding an item to the inventory.
    /// </summary>
    /// <param name="id"></param>
    public void AddItem(int id) {
        Item itemToAdd = database.FetchItemByID(id);

        if (itemToAdd.Stackable && ItemInInventory(itemToAdd)) {
            for (int i = 0; i < itemList.Count; i++) {
                if (itemList[i].ItemID == id) {
                    ItemData data = slotList[i].transform.GetChild(0).GetComponent<ItemData>();
                    data.Amount++;
                    data.transform.GetChild(0).GetComponent<Text>().text = data.Amount.ToString();
                    break;
                }
            }
        } else {
            for (int i = 0; i < itemList.Count; ++i) {
                if (itemList[i].ItemID == -1) {
                    itemList[i] = itemToAdd;
                    GameObject itemObj = Instantiate(inventoryItem);
                    ItemData itemData = itemObj.GetComponent<ItemData>();
                    itemData.Item = itemToAdd;
                    itemData.Slot = i;
                    itemData.Amount = 1;
                    itemObj.transform.SetParent(slotList[i].transform, false);
                    itemObj.GetComponent<Image>().sprite = itemToAdd.Sprite;
                    itemObj.name = itemToAdd.Title;
                    break;
                }
            }
        }
    }

    /// <summary>
    /// ItemInInventory() returns a boolean if the item already exists in the inventory.
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    bool ItemInInventory(Item item) {
        for (int i = 0; i < itemList.Count; i++) {
            if (itemList[i].ItemID == item.ItemID) {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Toggle() serves for displaying or hiding the inventory. Alpha being opacity.
    /// </summary>
    public void Toggle() {
        if (GetComponent<CanvasGroup>().interactable) {
            transform.GetComponent<CanvasGroup>().interactable = false;
            transform.GetComponent<CanvasGroup>().alpha = 0;
        } else {
            transform.GetComponent<CanvasGroup>().interactable = true;
            transform.GetComponent<CanvasGroup>().alpha = 1;
        }
    }
}
