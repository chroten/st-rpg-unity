﻿using UnityEngine;
using UnityEngine.Networking;
using System.Net;
using System.Net.Sockets;
using System.Collections;

public class NetworkInterface : MonoBehaviour {

    private string hostAddress;
    private int MAX_PLAYERS;
    private int playerCount = 0;
    private int hostId;
    private HostTopology topology;
    private int myReiliableChannelId;
    private ConnectionConfig config;
    private int connectionId;
    private int channelId;
    private byte[] recBuffer = new byte[1024];
    private int bufferSize = 1024;
    private int dataSize;
    private byte error;
    private const int port = 8888;

    // Use this for initialization
    void Start () {

    }

    public void Initialize(int players) {
        MAX_PLAYERS = players;
    }


    public string GetHostAddress() {
        return hostAddress;
    }

    public void CreateHostOnMyMachine() {
        CreateHost(GetIPv4Address());
    }

    public void CreateHost(string address) {

        hostAddress = address;

        config = new ConnectionConfig();
        myReiliableChannelId = config.AddChannel(QosType.Reliable);           // Slower, but assured reciept of messages
                                                                              // int myUnreliableChannelId = config.AddChannel(QosType.Unreliable); // Faster, but no guarantee of reciept
        topology = new HostTopology(config, MAX_PLAYERS);                     // Configs and max permitted connections
        hostId = NetworkTransport.AddHost(topology, port, hostAddress);  // Creates host, opens sockets

        Debug.Log("Opening socket with address " + hostAddress);

        

        // So far have added a new host on port 8888 which will allow 10 connections with the 2 types of channels
        // Atm I'm just going to use the Reliable config and have 5 connections

    }

    public void InitializeNetwork() {
        NetworkTransport.Init();
    }

    public int GetPlayerCount() {
        return playerCount;
    }

    public void IncrementPlayerCount() {
        playerCount++;
    }

    public void DecrementPlayerCount() {
        playerCount--;
    }

    public bool IsGameEmpty() {

        return !(playerCount > 0);

    }

    public bool IsGameFull() {
        return !(playerCount < MAX_PLAYERS);
    }

    public void SetHostAddress(string address) {
        hostAddress = address;
    }

    public void ConnectToCurrentHost() {
        byte error;
        connectionId = NetworkTransport.Connect(hostId, hostAddress, port, 0, out error);
        Debug.Log("Connect code ran");
    }

    public void DisconnectFromCurrentHost() {
        NetworkTransport.Disconnect(hostId, connectionId, out error);
        Debug.Log("Disconnect code ran");
    }

    public void ReceiveMessageFromHost() {

        NetworkEventType recData = NetworkTransport.ReceiveFromHost(hostId, out connectionId, out channelId, recBuffer, bufferSize, out dataSize, out error);

        switch (recData) {
            case NetworkEventType.Nothing:         //1
                //Debug.Log("No response from host");
                break;
            case NetworkEventType.ConnectEvent:    //2
                Debug.Log("A client connected");
                break;
            case NetworkEventType.DataEvent:       //3
                break;
            case NetworkEventType.DisconnectEvent: //4
                Debug.Log("A client disconnected");
                break;
        }

        System.Array.Clear(recBuffer, 0, recBuffer.Length);
    }



    private string GetIPv4Address() {
        string hostString = System.Net.Dns.GetHostName();
        IPHostEntry hostInfo = Dns.GetHostEntry(hostString);

        foreach (IPAddress ip in hostInfo.AddressList) {
            if (ip.AddressFamily == AddressFamily.InterNetwork) {
                return ip.ToString();
                // this is the ipv4
            }
        }

        return hostString;
    }

    // NetworkTransport.Disconnect(hostId, connectionId, out error);                                            *** Disconnect from a host ***
    // NetworkTransport.Send(hostId, connectionId, myReiliableChannelId, buffer, bufferLength, out error);      *** Send message to a host ***
}
