﻿using UnityEngine;
using System.Collections;
using System;

public class InterfaceMonoBehaviour : ExtendedMonoBehaviour {


    protected Player localPlayer;

    public void Initialize(Player player, UnityEngine.Object[] objectList) {
        localPlayer = player;
        Initialize(objectList);
    }

    public override void Initialize(UnityEngine.Object[] objectList) {

    }

}
