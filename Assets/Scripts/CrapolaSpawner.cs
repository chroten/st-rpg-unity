﻿using UnityEngine;
using System.Collections;

public class CrapolaSpawner : MonoBehaviour {

    public Transform crapola;
    float spacing = 10.0f;
    Vector3 bounds;

    // Use this for initialization
    void Start () {

        bounds = crapola.GetComponent<Renderer>().bounds.size * spacing;

        for (int x = -9; x < 9; x++)
        {
            for (int z = -9; z < 9; z++)
            {
                Instantiate(crapola, new Vector3(x * bounds.x, 30, z * bounds.z), Quaternion.identity);
            }
        }

    }
}
