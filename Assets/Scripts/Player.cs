﻿using UnityEngine;
using System.Collections;

public class Player : ProjectionMonoBehaviour {


    private float maxHealth = 100f;
    private float currentHealth = 0;

    private GameObject healthBar;

    private Vector2 mouseVector;
    private float moveSpeed = 200f;

    private Vector3 playerCameraDist;

    private Vector3 forward = Vector3.forward;
    private Vector3 strafe = Vector3.left;
    private Quaternion rotation;

    public override void Initialize(UnityEngine.Object[] objectList)  {

        healthBar = GameObject.Find("Bar");

        SetHealthBar(currentHealth / maxHealth);
        //playerCameraDist = cam.transform.position - transform.position;
        //playerCameraDist = inPlayerCamDist;

        rotation = Quaternion.AngleAxis(cam.transform.eulerAngles.y, Vector3.up);
        forward = rotation * forward;
        strafe = rotation * strafe;
    }

    void Update() {

        // Using camera.transform.Translate(strafe * camSpeed * Time.deltaTime) here
        // or any of the like seemingly causes the camera to be translated via it's onw
        // facing direction as opposed to the vector dictated by: forward  = rotation * forward;
        // Thus the equivalent transform for the player is interchangeable as it uses
        // the default facing vector whereas Camera is on it's owned fucked axis'

        cam.transform.position = transform.position + new Vector3(-300, 300, -300); // Solution that is 1bilx easier
        //cam.transform.Translate(playerCameraDist); // Solution that is 1bilx easier
        //cam.transform.position = playerCameraDist;
        //cam.transform.position = new Vector3(-704, 472, 291) - transform.position;
        PlayerFaceMouse();
    }

    public void SetPosition(Vector3 position) {
        transform.position = position;
    }

    public void SetCamera(Camera camera) {
        cam = camera;
    }

    public void MoveUp() {

        transform.position += forward * moveSpeed * Time.deltaTime;           // Could use transform.Translate(forward * moveSpeed * Time.deltaTime);
        //camera.transform.position += forward * camSpeed * Time.deltaTime;     // But we won't because it fucking looks nicer
    }

    public void MoveDown() {
        transform.position -= forward * moveSpeed * Time.deltaTime;
        //camera.transform.position -= forward * camSpeed * Time.deltaTime;
    }

    public void MoveLeft() {
        transform.position += strafe * moveSpeed * Time.deltaTime;
        //camera.transform.position += strafe * camSpeed * Time.deltaTime;
    }

    public void MoveRight() {
        transform.position -= strafe * moveSpeed * Time.deltaTime;
        //camera.transform.position -= strafe * camSpeed * Time.deltaTime;
    }

    void PlayerFaceMouse() {

        

        Vector2 screenPosPlayer = cam.WorldToScreenPoint(transform.position);
        Vector2 screenPosMouse = Input.mousePosition;
        mouseVector = screenPosPlayer - screenPosMouse;
        float sign = (screenPosPlayer.y > screenPosMouse.y) ? -1.0f : 1.0f;
        float angle = (Vector2.Angle(Vector2.right, mouseVector) * sign) - cam.transform.eulerAngles.y; // Camera is rotated 45 degrees to the World
                                                                                                        // so screen point calculations will, by default
                                                                                                        // be offset by 45 degrees. Hence we add
                                                                                                        // camera.transform.eulerAngles.y (45 degrees for our game)

        transform.rotation = Quaternion.Euler(0, angle, 0);
    }

    void FollowMouseOnClick() {
        if (Input.GetMouseButton(0)) {

            cam.transform.position = (new Vector3(transform.position.x - 100, cam.transform.position.y, transform.position.z - 100));
            transform.Translate(Vector3.left * moveSpeed * Time.deltaTime);
        }
    }

	
	

    void DecreaseHealth() {
        currentHealth -= 2f;
        SetHealthBar(currentHealth / maxHealth);
    }

    void IncreaseHealth() {
        currentHealth += 2f;
        SetHealthBar(currentHealth / maxHealth);
    }

    public void SetHealthBar(float hp) {
        if (currentHealth >= 0f && currentHealth <= maxHealth) {
            healthBar.transform.localScale = new Vector3(hp, healthBar.transform.localScale.y, healthBar.transform.localScale.z);
        }
    }

    public void OnCollisionEnter(Collision collision) {
        //cam.transform.rotation = Quaternion.AngleAxis(90, Vector3.up);
        if (collision.gameObject.name == "Crapola(Clone)") {
            Destroy(collision.gameObject);
            IncreaseHealth();

            transform.GetComponent<Rigidbody>().velocity = Vector3.zero;
            transform.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        }
    }
}
